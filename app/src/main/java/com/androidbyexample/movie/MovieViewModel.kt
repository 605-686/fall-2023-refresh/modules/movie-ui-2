package com.androidbyexample.movie

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movie.repository.MovieDatabaseRepository
import com.androidbyexample.movie.repository.MovieRepository
import com.androidbyexample.movie.screens.MovieList
import com.androidbyexample.movie.screens.Screen
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MovieViewModel(
    private val repository: MovieRepository,
): ViewModel(), MovieRepository by repository {
    // ##START 030-add-selection-to-vm
    private val _selectedIdsFlow = MutableStateFlow<Set<String>>(emptySet())
    val selectedIdsFlow: Flow<Set<String>> = _selectedIdsFlow.asStateFlow()
    // ##END

    // ##START 030-selection-management
    fun clearSelectedIds() {
        _selectedIdsFlow.value = emptySet()
    }
    fun toggleSelection(id: String) {
        if (id in _selectedIdsFlow.value) {
            _selectedIdsFlow.value -= id
        } else {
            _selectedIdsFlow.value += id
        }
    }
    // ##END

    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            // ##START 030-clear-on-screen-change
            clearSelectedIds()
            // ##END
            currentScreen = value.lastOrNull()
        }

    // ##START 030-current-screen
    // NOTE: We're keep this as a Compose State for comparison.
    //       You can use Compose state to expose anything from the view model,
    //       but our example will be using Flow from now on to demonstrate how
    //       the view model can be used without Compose, perhaps for other
    //       platforms such as iOS, desktop, web or command line
    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set
    // ##END

    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }

    // ##START 070-set-screen
    fun setScreen(screen: Screen) {
        screenStack = listOf(screen)
    }
    // ##END

    // ##START 050-vm-delete-selected-x
    fun deleteSelectedMovies() {
        viewModelScope.launch {
            deleteMoviesById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }
    fun deleteSelectedActors() {
        viewModelScope.launch {
            deleteActorsById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }
    fun deleteSelectedRatings() {
        viewModelScope.launch {
            deleteRatingsById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }
    // ##END

    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieDatabaseRepository.create(application)
                ) as T
            }
        }
    }
}
