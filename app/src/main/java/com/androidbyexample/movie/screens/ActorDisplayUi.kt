package com.androidbyexample.movie.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.components.Label
import com.androidbyexample.movie.repository.ActorWithFilmographyDto
import com.androidbyexample.movie.repository.MovieDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

// ##START 070-actor-display
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ActorDisplayUi(
    id: String,
    fetchActor: suspend (String) -> ActorWithFilmographyDto,
    onMovieClicked: (MovieDto) -> Unit,
) {
    var actorWithFilmography by remember { mutableStateOf<ActorWithFilmographyDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            actorWithFilmography = fetchActor(id)
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = actorWithFilmography?.actor?.name ?: stringResource(R.string.loading))
                }
            )
        }
    ) { paddingValues ->
        actorWithFilmography?.let { actorWithFilmography ->
            Column(
                modifier = Modifier
                    .padding(paddingValues)
            ) {
                Label(textId = R.string.name)
                Display(text = actorWithFilmography.actor.name)
                Label(
                    text = stringResource(
                        id = R.string.movies_starring,
                        actorWithFilmography.actor.name
                    )
                )

                List(
                    items = actorWithFilmography.filmography.sortedBy { it.movie.title },
                    onItemClicked = { onMovieClicked(it.movie) },
                    selectedIds = emptySet(),
                    onSelectionToggle = {},
                    onClearSelections = {},
                    modifier = Modifier.weight(1f)
                ) { role ->
                    Display(text = role.movie.title)
                }
            }
        }
    }
}
// ##END
