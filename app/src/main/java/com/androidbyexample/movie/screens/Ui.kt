package com.androidbyexample.movie.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movie.MovieViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val scope = rememberCoroutineScope()

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(
                id = screen.id,
                fetchMovie = viewModel::getMovieWithCast,
                // ##START 060-empty-actor-nav
                onActorClicked = { viewModel.pushScreen(ActorDisplay(it.actor.id)) }
                // ##END
            )
        }
        // ##START 070-new-displays
        is ActorDisplay -> {
            ActorDisplayUi(
                id = screen.id,
                fetchActor = viewModel::getActorWithFilmography,
                onMovieClicked = { viewModel.pushScreen(MovieDisplay(it.id)) }
            )
        }
        is RatingDisplay -> {
            RatingDisplayUi(
                id = screen.id,
                fetchRating = viewModel::getRatingWithMovies,
                onMovieClicked = { viewModel.pushScreen(MovieDisplay(it.id)) }
            )
        }
        // ##END
        MovieList -> {
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            // ##START 040-collect-selection
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            // ##END
            MovieListUi(
                movies = movies,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                // ##START 040-new-parameters
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                // ##START 050-ui-delete
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                // ##END
                // ##END
                // ##START 070-extra-params
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                // ##END
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        // ##START 070-new-lists
        ActorList -> {
            val actors by viewModel.actorsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            ActorListUi(
                actors = actors,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedActors = viewModel::deleteSelectedActors,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        RatingList -> {
            val ratings by viewModel.ratingsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            RatingListUi(
                ratings = ratings,
                onRatingClicked = { rating ->
                    viewModel.pushScreen(RatingDisplay(rating.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedRatings = viewModel::deleteSelectedRatings,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        // ##END
    }
}
