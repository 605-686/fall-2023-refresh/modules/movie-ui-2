package com.androidbyexample.movie.screens

sealed interface Screen

// ##START 070-new-screens
object MovieList: Screen
object ActorList: Screen
object RatingList: Screen
data class MovieDisplay(val id: String): Screen
data class ActorDisplay(val id: String): Screen
data class RatingDisplay(val id: String): Screen
// ##END
