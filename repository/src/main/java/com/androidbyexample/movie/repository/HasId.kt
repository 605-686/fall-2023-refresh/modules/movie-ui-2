package com.androidbyexample.movie.repository

// ##START 060-has-id
interface HasId {
    val id: String
}
// ##END
