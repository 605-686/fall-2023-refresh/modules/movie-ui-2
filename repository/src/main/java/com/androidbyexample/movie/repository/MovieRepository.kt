package com.androidbyexample.movie.repository

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<RatingDto>>
    val moviesFlow: Flow<List<MovieDto>>
    val actorsFlow: Flow<List<ActorDto>>

    suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto
    suspend fun getMovieWithCast(id: String): MovieWithCastDto
    suspend fun getActorWithFilmography(id: String): ActorWithFilmographyDto

    suspend fun insert(movie: MovieDto)
    suspend fun insert(actor: ActorDto)
    suspend fun insert(rating: RatingDto)

    // ##START 050-repo-delete-by-ids
    suspend fun deleteMoviesById(ids: Set<String>)
    suspend fun deleteActorsById(ids: Set<String>)
    suspend fun deleteRatingsById(ids: Set<String>)
    // ##END

    suspend fun resetDatabase()
}