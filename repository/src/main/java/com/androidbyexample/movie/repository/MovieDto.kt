package com.androidbyexample.movie.repository

import com.androidbyexample.movie.data.MovieEntity
import com.androidbyexample.movie.data.MovieWithCast
import com.androidbyexample.movie.data.RoleWithActor

// ##START 060-movie-has-id
data class MovieDto(
    override val id: String,
    val title: String,
    val description: String,
    val ratingId: String,
): HasId
// ##END

internal fun MovieEntity.toDto() =
    MovieDto(id = id, title = title, description = description, ratingId = ratingId)
internal fun MovieDto.toEntity() =
    MovieEntity(id = id, title = title, description = description, ratingId = ratingId)

data class MovieWithCastDto(
    val movie: MovieDto,
    val cast: List<RoleWithActorDto>,
)

// ##START 060-role-with-actor-has-id
data class RoleWithActorDto(
    val actor: ActorDto,
    val character: String,
    val orderInCredits: Int,
): HasId {
    override val id: String
        get() = "${actor.id}:$character"
}
// ##END

internal fun RoleWithActor.toDto() =
    RoleWithActorDto(
        actor = actor.toDto(),
        character = role.character,
        orderInCredits = role.orderInCredits,
    )

internal fun MovieWithCast.toDto() =
    MovieWithCastDto(
        movie = movie.toDto(),
        cast =
            rolesWithActors.map {
                it.toDto()
            }
    )