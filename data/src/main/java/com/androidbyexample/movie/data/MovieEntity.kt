package com.androidbyexample.movie.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

// Movie Entity - note that we set up a foreign key relationship with RatingEntity,
//   which is considered the owner of movies with that rating. (Probably not the best
//   way to represent this relationship, but I wanted to demonstrate a one-to-many
//   relationship with cascading deletion.
//   If we delete a Rating, we delete all associated movies. If we update the
//   Rating key, we update that key in all associated movies.
@Entity(
    indices = [
        Index(value = ["ratingId"])
    ],
    foreignKeys = [
        ForeignKey(
            entity = RatingEntity::class,
            parentColumns = ["id"],
            childColumns = ["ratingId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        )
    ]
)
data class MovieEntity(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var title: String,
    var description: String,
    var ratingId: String,
)

// POKO that groups a Movie with the actors that appear in it, and the
//   role played in the movie. This is a ONE-TO-MANY relation from the
//   MovieEntity to the RoleEntity, where we resolve the RoleEntity's actorId
//   to fetch the actual actor (a MANY-TO-ONE relation). The end result is turning
//   a MANY-TO-MANY relation into two ONE-TO-MANY relationships, where the MANY part
//   is the RoleEntity.

data class MovieWithCast(
    @Embedded
    val movie: MovieEntity,

    @Relation(
        entity = RoleEntity::class,
        parentColumn = "id",
        entityColumn = "movieId",
    )
    val rolesWithActors: List<RoleWithActor>,
)

data class RoleWithActor(
    @Embedded
    val role: RoleEntity,

    @Relation(
        parentColumn = "actorId",
        entityColumn = "id"
    )
    val actor: ActorEntity,
)
