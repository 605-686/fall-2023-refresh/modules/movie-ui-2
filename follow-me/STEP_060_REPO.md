---
title: Refactor time!
template: main-repo.html
---

Our app bakes the list support in with the Movie UI. We can make it much more reusable!

We're going to have two types of lists in the application:

   * Top-level lists of all movies, actors and ratings
   * Nested lists, such as actors starring in a movie (on the movie's display screen)

To create consistent list support, we need to separate the `LazyColumn` from the `Scaffold` and the `MovieListUi`. We want to keep all of the selection management, but make it more generic.

But there's a problem. If we make a generic `List` composable, something like

```kotlin
fun <T> List(
  items: List<T>,
  ...
) {
  ...
}
```

we have several spots that access the item's `id`. When the item type was explicitly `MovieDto`, we knew it had an id, but if the item type is a generic parameter `T`, we can no longer make that assumption.

To fix this, we create a 
{{ find("060-has-id", "`HasId` interface")}}
in the `repository` module. (You could create it in the `data` module, but we don't have a need for it that low.) We apply it to our
{{ find("060-movie-has-id", "`MovieDto`") }},
{{ find("060-actor-has-id", "`ActorDto`") }}, and
{{ find("060-rating-has-id", "`RatingDto`") }}.

We can now create a 
{{ find("060-factor-out-list", "generic `List`") }}
composable.

We can now replace the common list function in {{ find("060-use-list-in-movie-list", "`MovieListUi`")}}.

But now we can also use this list for the 
{{ find("060-movie-display-swap-out-list", "cast display") }}
in `MovieDisplayUi`, giving us a consistent-looking list... with a couple of hitches:

   * `RoleWithActorDto` doesn't implement `HasId`
   * `MovieDisplayUi` doesn't have a parameter to handle when an actor has been clicked
   * We don't want selections in the cast list, just clicks.

First, the `HasId`. We can add a
{{ find("060-role-with-actor-has-id", "derived `id`") }}
to `RoleWithActorDto`. The id here is a combination of movie id & character. (This *might* not be unique, but for our example purposes it's good enough).

Next, we add a parameter to handle
{{ find("060-actor-click", "clicks on roles") }} and just a for now
{{ find("060-empty-actor-nav", "empty handler for now") }} in `Ui`. (We'll add that navigation after we set up the other screens later.)

![Display with nicer list](screenshots/display-with-list.png){width=400}

## All code changes
