---
title: Selection Tracking
template: main-repo.html
---

Now let's set up item selection. We'll use the common approach of

   * Tapping the icon or long-pressing anywhere in the row toggles the selection
   * Tapping in the row anywhere _except_ the icon:
      * if any items are selected, toggle this item
      * if no items are selected, trigger navigation

First, we need to track the selections somewhere. Selections are data that are typically not kept between runs of an application, so there's no need to store them in the database.

But we usually want selections to persist across configuation changes (such as device rotation), so the View Model is a good choice to hold them.

We're going to use a Kotlin `Flow` to 
{{ find("030-add-selection-to-vm", "track") }}
the set of selected ids. `MutableStateFlow` holds onto a single emitted item (in this case, a `Set` of selected ids) that can be collected by the UI and its value accessed/set via its `value` property.

We're defining *two* `Flow`s here:

   * `_selectedIdsFlow` - this is *private* and can only be accessed by the view model. We don't want the UI to be able to directly modify things in the view model. The UI should call functions that give us more control over how updates are made.

   * `selectedIdsFlow` - we expose the `MutableStateFlow` as a non-mutable `Flow` by calling `asStateFlow()` on `_selectedIdsFlow`. This property is public and can be collected from the UI.

You may ask "why not use a `MutableState`? You could do that here instead of creating a `Flow`. It would look similar to our 
{{ find("030-current-screen", "`currentScreen`")}}
property, which we added before we started talking about `Flow`s.

`currentScreen` is exposed to the UI but only read-only. Using `private set`  makes the setter function private, only accessible to the view model.

The `currentScreen` approach code ends up being simpler as well - there's no need to collect the `Flow` in the UI; you just reference `selectedIds`.

In fact, I;d recommend converting `currentScreen` to use a `Flow` (but will leave it here for comparison).

So why use a `Flow` instead of a `MutableState`?

Part of this is "purity" - using `State` in a view model leaks some of the way we're implementing our UI into the view model.

!!! note

    Technically, `State` is part of compose-runtime, which is responsible for creating/managing trees and state. It isn't a "UI thing" and could be used for data management at any level.

    Unfortunately, there are some close ties with compose-ui, as well as the perception that `State` is a "UI thing". Thus, I recommend avoiding its use in view models (or further down, such as the repository or data modules).

The other part of this is potential reuse. If you use `State` inside your view model, your view model will only work where Compose can run. If you use `Flow`s, the view model will work wherever Kotlin can run.

This opens up the ability to use the same view model logic on Android, iOS, desktop app, web app, or even command-line ui. (My hope is that over time, Kotlin logic will be used across many platforms. Kotlin Multiplatform is getting there...)

To keep the purity, we'll use `Flow`s to expose data from the view model from now on. (I also hope to create a Kotlin Multiplatform course soon so we can explore this more!)

The last thing we need to do in the view model is add some 
{{ find("030-selection-management", "selection management") }}
functions. This will allow the UI to ask us to clear the selections and toggle a single selection.

!!! note

    It's also a good idea to clear the selections whenever the user
    {{ find("030-clear-on-screen-change", "changes screens") }}. We can do this
    by adding a call to `clearSelectedIds()` in the setter for our
    `screenStack`. Be sure to do this before the actual screen changes.
    
## All code changes
