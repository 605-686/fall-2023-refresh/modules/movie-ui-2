---
title: Movie UI - Lists
---

Now we'll use `LazyColumn` for more dynamic lists, and allow the user to select and delete list items.

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-2](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-2)
