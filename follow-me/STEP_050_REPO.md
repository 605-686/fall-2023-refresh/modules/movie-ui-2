---
title: Contextual Top Bar
template: main-repo.html
---

Now that we have selections, we can set up a contextial Top Bar to display the number of selections and an action to delete the selected items.

Currently we have static top bar that displays the page title and an action to reset the database. We can
{{ find("050-contextual-app-bar", "make this contextual") }}
based on whether or not we have selections.

This requires
{{ find("050-contextual-action-text", "some additional strings")}}
and a new
{{ find("050-delete-movies-param", "parameter")}} to delete the selected movies when the trash can is clicked.

When there are items selected, the contextual top bar displays the number of selected items, a back arrow to clear the selections (returning to the normal top bar), and a trash can icon to delete selected items.

![Contextual top bar](screenshots/contextual-top-bar.png){width=400}

We added code to clear selections when the back arrow is pressed, but what if the user performs a back gesture (or presses the back button)? We can
{{ find("050-back-handler", "handle back")}}
using a `BackHandler` (surprise, surprise), calling `onClearSelections`.

This back handler is only set up if there are any selected ids, and will override the back handler set up in `Ui`.

Now we need to add "delete by ids" support:

* Add {{ find("050-dao-delete-by-ids", "delete by id")}} support to the Dao
* Expose these {{ find("050-repo-delete-by-ids", "in `MovieRepository`") }}
* Pass them through in {{ find("050-passthrough", "in the `MovieDatabaseRepository`") }}
* They'll automatically be passed through the view model (because we delegated `MovieRepository`)
* {{ find("050-vm-delete-selected-x", "Add `deleteSelectedXXX`") }} functions for movies, actors and ratings to the view model.
* {{ find("050-ui-delete", "Pass `deleteSelectedMovies()`") }} as the function to use for `onDeleteSelectedMovies`

!!! note

    The `deleteSelectedXXX` functions in the view model launch a coroutine using the `viewModelScope`. This coroutine scope lives as long as the view model lives. If we used `rememberCoroutineScope` in `MovieListUi`, any coroutines launched would be canceled when the user leaves the screen.

  Phew - that's a long chain of deletes coming from the data layer! You shouldn't have to uninstall the app or increase the database version, as we only modified the DAO.

  When we run the application, we can now select movies and delete them using the trash can button. The list is automatically updated because we used a `Flow` as the return type for the getting the movie list. When the database has been changed, the `getMoviesFlow` query is re-executed, and the results emitted to the same `Flow`. The `Ui` collects the `Flow` into Compose state. Compose detects the state change and recomposes the UI. Poof!

## All code changes
