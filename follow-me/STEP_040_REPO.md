---
title: Selections in the UI
template: main-repo.html
---

Let's integrate selections into the UI.

First, we add {{ find("040-movie-list-selection-params", "parameters") }} to our `MovieListUi` composable function. These parameters give the function

  * `selectedIds` - the set of selections so we know what to highlight
  * `onSelectionToggle` - allows the function to request to toggle a selection
  * `onClearSelections` - allows the function to request all selections be cleared

We choose the color of each card based on its
{{ find("040-card-colors", "selection status") }}. (Note that `contentColorFor` will only work if the color passed in is defined in the theme. In this case, we're using `secondary` and `surface` colors from the theme so it'll work.)

We tell the `Card` which
{{ find("040-card-color-param", "colors to use")}} for its background, `containerColor` and forground, `contentColor`. The contentColor will be used for any nested text or icons.

We need to change the way clicks are handled to match our strategy. We'll do this for the

   * `Icon` - any clicks toggle the selection
   * `Card`
      * any long-clicks toggle the selection
      * any normal clicks
        * toggle the selection (if anything was selected), or
        * navigate to the movie (if nothing was selected)

We currently have an `onClick` defined on the `Card`, but because we want to handle both long and normal clicks, we need to switch to a 
{{ find("040-card-clicks", "`combinedClickable`") }} modifier.

We add a {{ find("040-icon-clicks", "`clickable`") }} modifier to the `Icon` to finish our click handling.

Finally, we 
{{ find("040-collect-selection", "collect the selection")}} from the view model and
{{ find("040-new-parameters", "wire up")}}
the new parameters in the `Ui` function.

This gives us a movie list that allows us to select movies!

![Selectable movies](screenshots/selection-1.png){width=400}

## All code changes
