---
title: Better Movie Icon
template: main-repo.html
---

Jetpack Compose comes with some useful Icons, but it's a rather short list.
You can see available icons at [Google Fonts](https://fonts.google.com/icons).

Some are in the 
`androidx.compose.material:material-icons-core` (pulled in when you include `androidx.compose.material3:material3` as a dependency), and the rest are available in `androidx.compose.material:material-icons-extended`, which you need to pull in yourself.

!!! warning

    The extended icons dependency is rather large, and you'll likely only need a few icons from it. You can shrink your app size (see
    [Shrink your app](https://developer.android.com/build/shrink-code#shrink-code)) or copy the source for the icons into your application. I recommend copying the source for the icons you need, as it reduces the build time.

Note that source appears to be missing or difficult to locate for some versions. At this point I'm using extended icons version 1.6.0-alpha06, which has source.

To include the movie icon:

   1. Set the {{ find("080-icons-extended-version", "version") }} of `material-icons-extended` you want in the version catalog.
   2. Define the {{ find("080-extended-icons", "library") }} in the version catalog.
   3. Include the {{ find("080-icons-extended-dep", "dependency")}} in your `app` module's build script. (Note that it's commented-out here, as the end result of this step has already copied the icon)
   4. Sync your build scripts (press the elephant icon on the Android Studio tool bar)
   5. {{ find("080-change-icon", "Change the icon") }} in `MovieListUi`.
   6. {{ find("080-change-icon-tab", "Change the icon") }} in the tab in `ListScaffold`.

At this point you can run the application you'll now see movie icons.

![Movie icons in app](screenshots/movie-icon.png){width=400}

Now we need to either shrink the app to remove all of the extra icons (this slows down the build!) or copy the source for the icons we need. We'll copy the source.

   1. Open `MovieListUi` in the editor
   2. Find `Icons.Default.Movie`
   3. Control-click on `Movie` to go to its source code. (If you don't see the source you'll need to try a different version of `material-icons-extended`)
   4. Copy the package name from the source
   5. Right-click on `java` under `app/src/main`
   6. Choose **New > Package**
   7. Paste the package name
   8. Copy the entire source (including the copyright header at the top) for the `Movie` icon
   9. Right-click the package you just created
   10. Choose **New > Kotlin Class/File**
   11. Type "Movie" as the name and choose **File**
   12. Delete the package statement and paste the source code

We now have a copy of the icon.

Next, remove the huge dependency by deleting or commenting out the {{ find("080-icons-extended-dep", "implementation line")}} from `app/build.gradle.kts`. Be sure to re-sync the Gradle info by clicking the elephant on the tool bar.

!!! note

    You do **not** need to delete the dependency information from the version catalog. This will allow you to more easily copy in other icons if you need to at a later date. I recommend leaving it and commenting out the dependency.

## All code changes
