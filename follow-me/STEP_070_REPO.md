---
title: Add Actors and Ratings
template: main-repo.html
---

To flesh out the app more, let's add in Actors and Ratings.

First, to make things simpler for the other lists, we 
{{ find("070-list-scaffold", "create a `ListScaffold`") }}
and 
{{ find("070-movie-list-using-list-scaffold", "use it in `MovieListUi`") }}

Now we add the new screens. These new screens are similar to the existing screens, but be careful of where you get the ids from. The `MovieDisplayUi`, for example, displays a list of `RoleWithActorDto`s, and when clicked, we need to call `it.actor.id` to get the proper id. (If you use just `it.id` here, the fetch will fail when looking up the actor)

  * Create the new {{ find("070-new-screens", "screen data")}}
  * Create the 
    {{ find("070-actor-list", "actor list")}} and
    {{ find("070-rating-list", "rating list")}} screens
  * Create the 
    {{ find("070-actor-display", "actor display")}} and
    {{ find("070-rating-display", "rating display")}} screens
  * Define the {{ find("070-new-screen-text", "necessary strings") }}
  * Add the new 
    {{ find("070-new-displays", "display")}} and
    {{ find("070-new-lists", "list")}} screen calls to `Ui`

When we run this version of the application, we can now drill deeper into the data. Pick a movie, then pick an actor, then pick movies starring that actor and so forth. Navigating back pops each screen off the stack, returning to the previous screen.

!!! note

    We're still using a Star icon for movies. We'll pick a better icon in the final step.

You'll notice that we currently have no means of navigating to the actor and rating list screens. To do this, we'll add tabs at the bottom of the list screens to jump to other lists.

Navigation-wise, we'll treat each of these jumps as restarting navigation, replacing the stack with the destination. This isn't necessary, but I wanted to demonstrate a navigation alternative.

At the bottom of the list scaffold we'll place three buttons which act like tabs.

We define {{ find("070-screen-button", "`ScreenSelectButton`")}} for these tabs.

Each tab takes a `Screen` as its target, and calls `onSelectListScreen()` passing that target screen when clicked. We use `NavigationBarItem`, provided by the Material 3 library, for the tab-like styling.

We place these buttons in a {{ find("070-nav-bar", "`NavigationBar`")}} in the `bottomBar` slot of the `Scaffold`. Once again, continue using a star as the icon for movies. We'll fix this in the next step.

These buttons require some extra parameters to track which is selected (`currentScreen`) and what to do when they're clicked (`onSelectListScreen`). We pass these up to all callers up through `Ui`.

We set these extra parameters in `Ui`; for example {{ find("070-extra-params", "when calling `MovieListUi`")}}. Note that this requires a new function in the view model to {{ find("070-set-screen", "explicitly set the screen stack")}}.

This creates a nice Ui that allows us to navigate starting from movies, actors or ratings. The screen tabs could be moved into a common base scaffold for all screens to allow instant jumping to a list from any screen (I'll leave that as an "exercise for the interested reader").

Now let's get a better movie icon...

## All code changes
