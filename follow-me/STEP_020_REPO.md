---
title: Convert to LazyColumn
template: main-repo.html
---

Our first step to creating a dynamic UI is to 
{{ find("020-convert-to-lazy-column", "change") }}
our normal `Column` to a `LazyColumn`.

!!! note

    `LazyColumn` comes with its own scrolling. To make it work properly, you must remove the `verticalScroll` modifier when switching from `Column` to
    `LazyColumn`. 

The content lambda for `LazyColumn` contains an `items` call. Here we specify the list of items to use, and how to map an id to each. We know `MovieDto` has an id, so we can just use it as the `id` for the displayed row. Using a key allows `LazyColumn` to better optimize its recomposition of its rows, and later we'll see that it also supports animation of added or deleted rows.

## All code changes
